class PostsController < ApplicationController
  #http_basic_authenticate_with name: "admin", password: "adminrubyblog",
    #except: [:index, :show]
  before_action :authenticate_user!, except: [:index, :show]
  def index
    @topPosts = Array.new
    Post.all.each do |post|
      @topPosts.push(post)
    end

    n = @topPosts.length - 1
    n.times do |i|
      min_index = i
      for j in (i + 1)..n
        min_index = j if @topPosts[j].comments.count < @topPosts[min_index].comments.count
      end
      @topPosts[i], @topPosts[min_index] = @topPosts[min_index], @topPosts[i] if min_index != i
    end

    @topPosts.sort_by {|obj| obj.comments.count}
    @topPosts = @topPosts.last(3)

    @posts = Post.order(id: :desc)
  end

  def search
    
    @req = params["post"]["requeststring"]
    puts "!!!!!!!!!!!!!!!"
    puts @req
    puts "!!!!!!!!!!!!!!!"
    @posts = Array.new
    Post.all.each do |p|
      if p.title.nil? || p.body.nil?
        puts "err"

      elsif p.title.include? @req
        @posts.push(p)
      elsif p.body.include? @req
        @posts.push(p)
      end

    end

  end



  def new
    @post=Post.new
  end

  def show
    @post = Post.find(params[:id])
  end

  def edit
    @post = Post.find(params[:id])
  end

  def create
    @user = User.find(current_user.id)
    #render plain: params[:post].inspect
    @post = @user.posts.create(postValidate)
    @post.user_id = current_user.id

    if(@post.save)
      redirect_to @post #call show()
    else
      render 'new'
    end
  end

  def update
    @post = Post.find(params[:id])

    if(@post.update(postValidate))
      redirect_to @post #call show()
    else
      render 'edit'
    end
  end

  def destroy
    @post = Post.find(params[:id])
    @post.title = "deleted"
    @post.save
    #@post.destroy!
    if(@post.destroy)
      redirect_to posts_path #call show()
    else
      redirect_to @post
    end
  end

  


  def change_user_avatar_method
    current_user.image.attach imgValidate[:image]
    current_user.save

    redirect_to action: 'index'
  end


  private def postValidate
    params.require(:post).permit(:title,:body,:image)
  end

  private def imgValidate
    params.require(:avatar).permit(:image)
  end


end
