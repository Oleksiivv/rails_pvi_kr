class CommentsController < ApplicationController
  #http_basic_authenticate_with name: "admin", password: "adminrubyblog",
    #except: [:index, :show]
  before_action :authenticate_user!

  def create #called by submit
    @post = Post.find(params[:post_id])
    @comment = @post.comments.create(commentValid)
    @comment.userName = current_user.email
    @comment.save

    redirect_to post_path(@post)
  end

  private def commentValid
    params.require(:comment).permit(:userName, :body)
  end
end
