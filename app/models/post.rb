class Post < ApplicationRecord
  has_many :comments
  has_one_attached :image
  validates :title, presence: true, length: {minimum: 5}
  belongs_to :user
end
