Rails.application.routes.draw do
  devise_for :users
  devise_scope :user do
    get '/users/sign_out' => 'devise/sessions#destroy'
  end
  root 'posts#index', as: "home"

  get 'about' => 'pages#about', as: "about"
  post 'search' => 'posts#search', as: "search"
  get 'contacts' => 'pages#contacts', as: "contacts"
  get 'profile' => 'pages#curruserposts', as: "profile"
  get 'change_user_avatar' => 'pages#change_user_avatar', as: 'change_user_avatar'
  post 'change_user_avatar_method' => 'posts#change_user_avatar_method', as: 'change_user_avatar_method'

  resources :posts do
    resources :comments
  end
end
